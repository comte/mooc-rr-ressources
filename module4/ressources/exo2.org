# -*- mode: org -*-
#+TITLE:     Module 4 / Exercice 2
#+DATE: June, 2018
#+STARTUP: overview indent
#+OPTIONS: num:nil toc:t
#+PROPERTY: header-args :eval never-export

* Exercice 2 : L'importance de l'environnement
Dans cet exercice, nous vous proposons de reprendre l'exercice
précédent mais en mettant à jour l'environnement de calcul. En effet,
nous avons rencontré des surprises en préparant ce MOOC puisqu'il nous
est arrivé d'avoir des résultats différents entre nos machines et
l'environnement Jupyter que nous avions mis en place pour le MOOC. Ça
sera peut-être également votre cas !

1. Pour ceux qui ont suivi le parcours Jupyter, recréez
   l'environnement du MOOC sur votre propre machine en suivant les
   instructions données 
   [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/4ab5bb42ca1e45c8b0f349751b96d405][dans les ressources de la section 4A du module 2]].
2. Vérifiez si vous obtenez bien les mêmes résultats que ceux
   attendus.
3. Mettez à jour (vers le haut ou vers la bas) cet environnement et
   vérifiez si vous obtenez les mêmes résultats.

Comme précédemment, vous mettrez à jour le [[https://app-learninglab.inria.fr/gitlab/moocrr-session2/moocrr-reproducibility-study/blob/master/results.md][tableau]] et vous discuterez
sur le forum des succès et des échecs que vous aurez rencontrés.

* Exercice 2: The importance of the environment
In this exercise, we ask you to redo the preceding exercise after
updating the computational environment. When preparing this MOOC, we
had a few surprises due to different results on our own computers and
on the Jupyter environment that we had installed for the MOOC. Maybe
that will happen to you as well!

1. For those you followed the Jupyter path, re-create the MOOC's Jupyter environment on your own computer by following the instructions given
   [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/4ab5bb42ca1e45c8b0f349751b96d405][in the resource section of sequence 4A of module 2]].
2. Check if you get the same results as in the MOOC environment.
3. Update this environment, increasing or decreasing some package's version numbers, and check if the results are still the same.

As before, you can add your observations to the [[https://app-learninglab.inria.fr/gitlab/moocrr-session2/moocrr-reproducibility-study/blob/master/results.md][table]] and discuss your successes and failures on the forum.
